# UTFPR-TSD Federal University of Technology - Paraná - Tattoo Sketch Dataset



## Introduction

The dataset comprises selected tattoo photos (from previously selected classes) and a set of drawings (sketches) for each tattoo, most handmade by different users.

## Tattoo images collection
The photos of tattoos were collected by means of web scraping methods on two well-known platforms: Google Images and Flickr. Each image was then manually cropped to center the tattoo, leaving aside the background of the photograph, and standardized in width (300 pixels), keeping a proportional height. The total number of classes is 20, and the class names are the following: spider, gun, dog, cobra, owl, crucifix, knife, man_face, woman_face, flower, cat, lion, wizard, eye, clown, bird, fish, octopus, clock, yin_yang.

A total of 100 images per class was collected. Therefore the final dataset had 2000 original images of tattoos. 

![Examples of tattoo images of the dataset.](DataTattoo.jpeg)

## Tattoo sketch collection

The sketches were collected using a web tool, where anonymous people drawed a tattoo and then updated the drawn. The classes used for collect sketches were 11 and the classes are the following: spider, gun, owl, cat, flower, eye, fish, clock, bird, cobra, crucifix. And we selected 60 images from the 100 that we had from each class to create an sketch.

In total we obtained 60 sketches per class, one for each image, giving a total of 660 sketches.

![Examples of tattoo images of the dataset.](DataTattooSketches.jpeg)

## Related Works

If you use of the UTFPR-TSD dataset, please cite the following reference in any publications:


Solari, B. (2021). Sketch-Based multimodal image retrieval using deep learning (Master's thesis, Universidade Tecnológica Federal do Paraná).

```
@mastersthesis{solari2021sketch,
  title={Sketch-Based multimodal image retrieval using deep learning},
  author={Solari, Brenda},
  year={2021},
  school={Universidade Tecnol{\'o}gica Federal do Paran{\'a}}
}
```
